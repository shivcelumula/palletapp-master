//
//  WebServices.m
//  PalletApp
//
//  Created by Celumula,Shiva Teja on 7/3/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "WebServices.h"


@implementation WebServices

-(BOOL)validateLogin:(NSString*)empID password:(NSString*)password;
{
    NSMutableArray *responseArray = [[NSMutableArray alloc] init];
    
    NSURL *url = [NSURL URLWithString:@""];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSError *err;
    
    if (data != nil) {
        
        responseArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
        
        for (NSDictionary *dic in responseArray) {
            [locationArray addObject:[dic objectForKey:@"Location1"]];
        }
        NSLog(@"responseArray : %@", responseArray);
        
        url = [NSURL URLWithString:@""];
        data = [NSData dataWithContentsOfURL:url];
        
        if (data != nil) {
            
            responseArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];

            return YES;
            
        }
        else
        {
            return NO;
        }
        
    }
    else
    {
        return NO;
    }
    
    
    //********************** Alternate Webservices *************************//
    
    /*
    
    
    // create a soap Message which is given in your required web service
    
    NSMutableData *myWebData;
    
    NSString *soapMessage=@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
    "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
    "<soap:Body>\n"
    "<GetCategory xmlns=\"http://tempuri.org/\" />\n"
    "</soap:Body>\n"
    "</soap:Envelope>";
    
    // create a url to your asp.net web service.
    NSURL *tmpURl=[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.32.10/Itavema/Admin/Itavema_Service.asmx"]];
    
    // create a request to your asp.net web service.
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:tmpURl];
    
    // add http content type - to your request
    [theRequest addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    // add  SOAPAction - webMethod that is going to be called
    [theRequest addValue:@"http://tempuri.org/GetCategory" forHTTPHeaderField:@"SOAPAction"];
    
    // count your soap message lenght - which is required to be added in your request
    NSString *msgLength=[NSString stringWithFormat:@"%lu",(unsigned long)[soapMessage length]];
    // add content length
    [theRequest addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    
    // set method - post
    [theRequest setHTTPMethod:@"POST"];
    
    // set http request - body
    [theRequest setHTTPBody:[soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    // establish connection with your request & here delegate is self, so you need to implement connection's methods
    NSURLConnection *con=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    // if connection is established
    if(con)
    {
        myWebData=[NSMutableData data];
        // here -> NSMutableData *myWebData; -> declared in .h file
    }
    
     */
    
    return YES;
}

-(BOOL)getMetaData
{
    
    NSMutableDictionary *responseDic = [[NSMutableDictionary alloc] init];
    
    NSURL *url = [NSURL URLWithString:[WebConstants getProductUrl]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSError *err;
    
    if (data != nil) {
        
        responseDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
        
        for (NSDictionary *dic in responseDic) {
            [productsArray addObject:[dic objectForKey:@"ProductDescription"]];
            [prodPackUnitArray addObject:[dic objectForKey:@"PackingUnit"]];
            
            [productDic setObject:[NSString stringWithFormat:@"%@   -   %@", [dic objectForKey:@"ProductDescription"], [dic objectForKey:@"PackingUnit"]] forKey:[dic objectForKey:@"ProductId"]];
        }
    }
    else
    {
        return NO;
    }
    
    
    
    url = [NSURL URLWithString:[WebConstants getLocationUrl]];
    data = [NSData dataWithContentsOfURL:url];
    
    if (data != nil) {
        
        responseDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
        
        for (NSDictionary *dic in responseDic) {
            
            [locationArray addObject:[dic objectForKey:@"Locationid"]];
        }
    }
    else
    {
        return NO;
    }
    
    
//    url = [NSURL URLWithString:[WebConstants getOrderUrl]];
//    data = [NSData dataWithContentsOfURL:url];
//    
//    if (data != nil) {
//        
//        responseDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
//        
//        for (NSDictionary *dic in responseDic) {
//            
//            [orderArray addObject:[NSString stringWithFormat:@"%@", [dic objectForKey:@"Orderid"]]];
//        }
//        
//    }
//    
//    else
//    {
//        return NO;
//    }
//    
//    url = [NSURL URLWithString:[WebConstants getOrderProductUrl]];
//    data = [NSData dataWithContentsOfURL:url];
//    
//    if (data != nil) {
//        
//        responseDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
//        NSMutableDictionary *addDic = [[NSMutableDictionary alloc] init];
//        
//        for (NSDictionary *dic in responseDic) {
//    
//            [addDic setValue:[dic objectForKey:@"Orderid"] forKey:@"Orderid"];
//            [addDic setValue:[dic objectForKey:@"productId"] forKey:@"productId"];
//            [addDic setValue:[dic objectForKey:@"OrderProductQuantity"] forKey:@"OrderProductQuantity"];
//            
//            [orderProductArray addObject:addDic];
//            
//            addDic = [NSMutableDictionary dictionaryWithDictionary:nil];
//        }
//        
//    }
//    
//    else
//    {
//        return NO;
//    }
    
    
    
    url = [NSURL URLWithString:[WebConstants getUsersUrl]];
    data = [NSData dataWithContentsOfURL:url];
    
    if (data != nil) {
        
        responseDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
        NSMutableDictionary *userDic = [[NSMutableDictionary alloc] init];
        
        for (NSDictionary *dic in responseDic) {
            
            [userDic setValue:[dic objectForKey:@"Userid"] forKey:@"UserId"];
            [userDic setValue:[dic objectForKey:@"Userpassword"] forKey:@"Userpassword"];
            [userDic setValue:[dic objectForKey:@"FirstName"] forKey:@"FirstName"];
            [userDic setValue:[dic objectForKey:@"LastName"] forKey:@"LastName"];
            
            [usersArray addObject:userDic];
            
            userDic = [NSMutableDictionary dictionaryWithDictionary:nil];
        }
        
    }
    else
    {
        return NO;
    }
    
    
    NSLog(@"productsArray :%@, \nlocationArray :%@, \nUsersArray :%@,  \nproductDic : %@", productsArray, locationArray, usersArray, productDic);
    return YES;
    
}


-(BOOL)postStorePalletData:(NSMutableArray*)dataArray
{
    NSError *error;

    NSDictionary *postDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                              dataArray[0], @"Storepalletid",
                              dataArray[1], @"Numberofstoredboxes",
                              dataArray[3], @"Productid",
                              dataArray[2], @"Movementid",
                              nil];
    
    
//     NSString *post = [NSString stringWithFormat:[WebConstants createBoxDataStr],dataArray[0], dataArray[1],@"",dataArray[2], dataArray[3], dataArray[4], dataArray[5], dataArray[6]];
   
//     NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
   
    
     NSData *postData = [NSJSONSerialization dataWithJSONObject:postDict options:kNilOptions error:&error];
    
     NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
   
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
   
     [request setURL:[NSURL URLWithString:[WebConstants postStorePalletUrl]]];
     [request setHTTPMethod:@"POST"];
     [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
     [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
     [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
     [request setHTTPBody:postData];
   
     NSURLResponse *response;
     NSData * results = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (results !=nil) {
        
        NSArray * responseArray = [NSJSONSerialization JSONObjectWithData:results options:0 error:&error];
        NSLog(@"responseArray : %@", responseArray);
        
        return YES;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Internet / Server error. Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }
    
    
}

-(BOOL)postShipPalletData:(NSMutableArray*)dataArray
{
    NSError *error;
    
    NSDictionary *postDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                              dataArray[0], @"ShippingPalletid",
                              dataArray[1], @"Orderid",
                              dataArray[2], @"Movementid",
                              nil];
    
    
    //     NSString *post = [NSString stringWithFormat:[WebConstants createBoxDataStr],dataArray[0], dataArray[1],@"",dataArray[2], dataArray[3], dataArray[4], dataArray[5], dataArray[6]];
    
    //     NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:postDict options:kNilOptions error:&error];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:[WebConstants postShipPalletUrl]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSData * results = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (results !=nil) {
        
        NSArray * responseArray = [NSJSONSerialization JSONObjectWithData:results options:0 error:&error];
        NSLog(@"responseArray : %@", responseArray);
        
        return YES;
    
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Internet / Server error. Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }
    
}
@end
