//
//  CreatePalletViewController.m
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "CreatePalletViewController.h"

@interface CreatePalletViewController ()

@end

@implementation CreatePalletViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    palletID = @"";
    noOfBoxes = @"";
    location = @"";
    product = @"";
    order = @"";
    shipProductsArray = [NSMutableArray arrayWithArray:@[]];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
