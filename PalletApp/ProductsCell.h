//
//  ProductsCell.h
//  Tables-Beta
//
//  Created by Sebastian  Kline on 1/24/14.
//  Copyright (c) 2014 Kline Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *storeProductLbl;
@property (nonatomic, weak) IBOutlet UILabel *storeUnitWeightLbl;

@property (weak, nonatomic) IBOutlet UILabel *shipProductLbl;
@property (weak, nonatomic) IBOutlet UILabel *shipUnitWeightLbl;

@end
