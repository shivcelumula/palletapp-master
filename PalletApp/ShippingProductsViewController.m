//
//  ShippingProductsViewController.m
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "ShippingProductsViewController.h"

@interface ShippingProductsViewController ()

@end

@implementation ShippingProductsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _index = 0;
    
//    productsArray = [NSMutableArray arrayWithArray:@[@"Chicken", @"Cream Mix", @"Sriracha Sauce", @"Cheese", @"Buttermilk", @"Meat"]];
//    
//    prodPackUnitArray = [NSMutableArray arrayWithArray:@[@"20/80 Oz", @"100/25 Oz", @"100/20 Oz", @"25/120 Oz", @"25/40 Oz", @"20/40 Oz"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return [orderProductsListArray count];
    
    return [productsArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    static NSString *CellIdentifier = @"Cell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    static NSString *CellIdentifier = @"ShipProductsCell";
    ProductsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    cell.shipProductLbl.text = [productsArray objectAtIndex:indexPath.row];
    cell.shipUnitWeightLbl.text = [prodPackUnitArray objectAtIndex:indexPath.row];
    
    cell.shipProductLbl.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.shipProductLbl.textAlignment = NSTextAlignmentCenter;
    
    cell.shipUnitWeightLbl.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.shipUnitWeightLbl.textAlignment = NSTextAlignmentCenter;
    
//    for (NSString *str in shipProductsArray) {
//        if ([str isEqualToString:[orderProductsListArray objectAtIndex:indexPath.row]]) {
//            
//            _index = indexPath;
//        }
//    }
//    
//    [tableView selectRowAtIndexPath:_index animated:YES scrollPosition:UITableViewScrollPositionNone];
//    
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BlankCell-Details.png"]];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [shipProductsArray addObject:[orderProductsListArray objectAtIndex:indexPath.row]];
    
    [shipProductsArray addObject:[productsArray objectAtIndex:indexPath.row]];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [shipProductsArray removeObject:[orderProductsListArray objectAtIndex:indexPath.row]];
    [shipProductsArray removeObject:[productsArray objectAtIndex:indexPath.row]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
