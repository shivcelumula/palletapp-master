//
//  PalletAppDelegate.h
//  PalletApp
//
//  Created by Shivzz on 7/24/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PalletAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
