//
//  main.m
//  PalletApp
//
//  Created by Shivzz on 7/24/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PalletAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PalletAppDelegate class]));
    }
}
