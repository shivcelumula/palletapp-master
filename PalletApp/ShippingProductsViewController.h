//
//  ShippingProductsViewController.h
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Variables.h"
#import "ProductsCell.h"

@interface ShippingProductsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property NSIndexPath* index;

@end
