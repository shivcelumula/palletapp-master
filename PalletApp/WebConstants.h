//
//  WebConstants.h
//  PalletApp
//
//  Created by Shivzz on 7/28/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebConstants : NSObject

+(NSString*)validateUrl;
+(NSString*)getProductUrl;
+(NSString*)getLocationUrl;
+(NSString*)getOrderUrl;
+(NSString*)getOrderProductUrl;
+(NSString*)getUsersUrl;
+(NSString*)postStorePalletUrl;
+(NSString*)postShipPalletUrl;


@end
