//
//  StoreProductsViewController.h
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Variables.h"
#import "ProductsCell.h"

@interface StoreProductsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
