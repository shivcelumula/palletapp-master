//
//  ShippingPalletDetailViewController.h
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Variables.h"


@interface ShippingPalletDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property UITableView *dropDownTableView;

@property (weak, nonatomic) IBOutlet UILabel *palletIdLbl;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLbl;
@property (weak, nonatomic) IBOutlet UITextField *locationTF;

@property (weak, nonatomic) IBOutlet UITableView *productsTableView;

- (IBAction)dropDownAction:(UIButton *)sender;
- (IBAction)doneAction:(UIButton *)sender;


@end
