//
//  LoginViewController.m
//  PalletApp
//
//  Created by Shivzz on 7/24/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    palletIndex = 0;
    
    _webServices = [[WebServices alloc] init];
    
    locationArray = [[NSMutableArray alloc] init];
    orderArray = [[NSMutableArray alloc] init];
    
    orderArray = [NSMutableArray arrayWithArray:@[@"00123", @"00234", @"00345", @"00456",@"00567", @"00678"]];
    
    //locationArray = [NSMutableArray arrayWithArray:@[@"A001-02", @"A002-03", @"B003-04", @"B004-05",@"C001-04", @"C002-05"]];
    
    productsArray = [[NSMutableArray alloc] init];
    prodPackUnitArray = [[NSMutableArray alloc] init];
    productDic = [[NSMutableDictionary alloc] init];
    
    orderProductArray = [[NSMutableArray alloc] init];
    orderProductsListArray = [[NSMutableArray alloc] init];
    
    shipProductsArray = [[NSMutableArray alloc] init];
    shipPalletArray = [[NSMutableArray alloc] init];
    storePalletArray = [[NSMutableArray alloc] init];
    
    usersArray = [[NSMutableArray alloc] init];
    
//    [usersArray addObjectsFromArray:@[@{@"FirstName": @"Will",@"LastName" : @"Smith", @"UserId" : @"E1234",@"Userpassword" : @"E1234"}, @{@"FirstName": @"John",@"LastName" : @"David", @"UserId" : @"E2345",@"Userpassword" : @"E2345"}, @{@"FirstName": @"Sarah",@"LastName" : @"Williams", @"UserId" : @"E3456",@"Userpassword" : @"E3456"}]];
    
    // Webservice call
    [_webServices getMetaData];

    
    
    _indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicator.frame = CGRectMake(142.0, 240.0, 50.0, 50.0);
    _indicator.center = self.view.center;
    [self.view addSubview:_indicator];
    [_indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

}


- (IBAction)loginActionButton:(UIButton *)sender {
    
    if (![_empIdTF.text isEqualToString:@""] && ![_passwordTF.text isEqualToString:@""]) {

        if([self validateLogin:_empIdTF.text password:_passwordTF.text])
        {
            EmpHomeViewController *empHomeVC = (EmpHomeViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"EmpHomeViewController"];
            
            empHomeVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:empHomeVC animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Invalid login. Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter employee ID and password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

-(BOOL)validateLogin:(NSString*)empID password:(NSString *)password
{
    for (NSMutableDictionary *dic in usersArray) {
        
        if ([empID isEqualToString:[dic objectForKey:@"UserId"]] && [password isEqualToString:[dic objectForKey:@"Userpassword"]]) {
            return YES;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_empIdTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    return YES;
}


@end
