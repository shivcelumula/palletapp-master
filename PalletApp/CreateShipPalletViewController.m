//
//  CreateShipPalletViewController.m
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "CreateShipPalletViewController.h"

@interface CreateShipPalletViewController ()

@end

@implementation CreateShipPalletViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _webServices = [[WebServices alloc] init];

    _orderIndex = 0;
    _buttonIndex = 0;
    
    _dropDownTableView=[[UITableView alloc]init];
    _dropDownTableView.dataSource=self;
    _dropDownTableView.delegate=self;
    _dropDownTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _dropDownTableView.backgroundColor = [UIColor lightGrayColor];
    [_dropDownTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [_dropDownTableView reloadData];
    [self.view addSubview:_dropDownTableView];
    _dropDownTableView.hidden = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    _shipPalletIdTF.text = palletID;
    _orderTF.text = order;
    _locationTF.text = location;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    palletID = _shipPalletIdTF.text;
    order = _orderTF.text;
    location = _locationTF.text;
    
    orderProductsListArray = [NSMutableArray arrayWithArray:nil];
    for (NSMutableDictionary *dic in orderProductArray) {
        
        if ([order isEqualToString:[NSString stringWithFormat:@"%@", [dic objectForKey:@"Orderid"]]]) {
            
            [orderProductsListArray addObject:[NSString stringWithFormat:@"%@    -    %@", [dic objectForKey:@"OrderProductQuantity"], [productDic objectForKey:[dic objectForKey:@"productId"]]]];
    }
    
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_buttonIndex == 100)
        return [orderArray count];
    else
        return [locationArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier  forIndexPath:indexPath] ;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (_buttonIndex == 100)
        cell.textLabel.text=[orderArray objectAtIndex:indexPath.row];
    else
        cell.textLabel.text=[locationArray objectAtIndex:indexPath.row];
    
    cell.textLabel.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_dropDownTableView setHidden:YES];
    
    if (_buttonIndex == 100) {
        
        _orderIndex = (int)indexPath.row;
        _orderTF.text = [orderArray objectAtIndex:_orderIndex];
    }
    else{
        _locationTF.text = [locationArray objectAtIndex:indexPath.row];
    }
    
}

- (IBAction)shipScanButtonAction:(UIButton *)sender {
    
    int randomNumber = 100 + rand() % (1000-100);
    _shipPalletIdTF.text = [NSString stringWithFormat:@"ShP0%d",randomNumber];
}

- (IBAction)doneButtonAction:(UIButton *)sender {
        
//    if (!([_shipPalletIdTF.text isEqualToString:@""] || [_orderTF.text isEqualToString:@""] || [_locationTF.text isEqualToString:@""] )) {
    
        [shipPalletArray addObject:[NSMutableArray arrayWithArray:@[_shipPalletIdTF.text, _orderTF.text, _locationTF.text,shipProductsArray]] ];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Order is done" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        CreatePalletViewController *createPVC = (CreatePalletViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePalletViewController"];
        
        createPVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:createPVC animated:YES completion:nil];
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter all details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//    }
}

- (IBAction)palletDoneAction:(UIButton *)sender {
    

    
    if (!([_shipPalletIdTF.text isEqualToString:@""] || [_orderTF.text isEqualToString:@""] || [_locationTF.text isEqualToString:@""] )) {
        
        
        shipPalletArray = [NSMutableArray arrayWithArray:@[_shipPalletIdTF.text, _orderTF.text, [NSString stringWithFormat:@"MOV%@",_shipPalletIdTF.text]]];
        
//        [shipPalletArray arrayByAddingObjectsFromArray:@[_shipPalletIdTF.text, _orderTF.text, [NSString stringWithFormat:@"MOV%@",_shipPalletIdTF.text]]];
        
        
        if ([_webServices postShipPalletData:shipPalletArray ]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Shipping Pallet has been created" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
//            CreatePalletViewController *createPVC = (CreatePalletViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePalletViewController"];
//            
//            createPVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//            [self presentViewController:createPVC animated:YES completion:nil];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Internet / Server error. Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Shipping Pallet has been created" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//        
        _shipPalletIdTF.text = @"";
        _locationTF.text = @"";
        shipProductsArray = [NSMutableArray arrayWithArray:@[]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter all details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    

}

- (IBAction)dropDownAction:(UIButton *)sender {
    
    if (sender.tag == 100) {
        
        _dropDownTableView.frame = CGRectMake(70, 144, 175, 140);
        _buttonIndex = 100;
    }
    else if (sender.tag == 101){
        
        _dropDownTableView.frame = CGRectMake(70, 262, 175, 140);
        _buttonIndex = 101;
    }
    
    [_dropDownTableView reloadData];
    _dropDownTableView.hidden = NO;
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_shipPalletIdTF resignFirstResponder];
    [_orderTF resignFirstResponder];
    [_locationTF resignFirstResponder];
    
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _dropDownTableView.hidden = YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
