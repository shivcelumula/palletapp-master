//
//  Variables.h
//  PalletApp
//
//  Created by Shivzz on 7/28/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *palletID;
NSString *noOfBoxes;
NSString *location;
int locationIndex;
int palletIndex;
NSString *product;
NSString *order;

NSMutableArray *locationArray;
NSMutableArray *orderArray;
NSMutableArray *shipProductsArray;

NSMutableArray *storePalletArray;
NSMutableArray *shipPalletArray;

NSMutableArray *productsArray;
NSMutableArray *prodPackUnitArray;
NSMutableDictionary *productDic;


NSMutableArray *orderProductArray;
NSMutableArray *orderProductsListArray;

NSMutableArray *usersArray;




