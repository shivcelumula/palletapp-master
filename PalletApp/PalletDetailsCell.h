//
//  PalletDetailsCell.h
//  PalletApp
//
//  Created by Shivzz on 9/26/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PalletDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *palletIDLbl;

@property (weak, nonatomic) IBOutlet UILabel *locationLbl;

@end
