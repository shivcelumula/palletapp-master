//
//  WebConstants.m
//  PalletApp
//
//  Created by Shivzz on 7/28/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "WebConstants.h"

@implementation WebConstants

NSString *validateUrl = @"";
NSString *getProductUrl = @"http://192.168.1.122:52726/api/Product";
NSString *getLocationUrl = @"http://192.168.1.122:52726/api/Location";
NSString *getOrderUrl = @"http://192.168.1.122:52726/api/Order";
NSString *getOrderProductUrl = @"http://192.168.1.122:52726/api/OrderProduct";
NSString *getUsersUrl = @"http://192.168.1.122:52726/api/Users";

NSString *postStorePalletUrl = @"http://192.168.1.122:52726/api/Storepallet";
NSString *postShipPalletUrl = @"http://192.168.1.122:52726/api/ShippingPallet";


+(NSString*)validateUrl
{
    return validateUrl;
}


+(NSString*)getProductUrl
{
    return getProductUrl;
}

+(NSString*)getLocationUrl
{
    return getLocationUrl;
}

+(NSString*)getOrderUrl
{
    return getOrderUrl;
}

+(NSString*)getOrderProductUrl
{
    return getOrderProductUrl;
}

+(NSString*)getUsersUrl
{
    return getUsersUrl;
}

+(NSString*)postStorePalletUrl
{
    return postStorePalletUrl;
}
+(NSString*)postShipPalletUrl
{
    return postShipPalletUrl;
}
@end




