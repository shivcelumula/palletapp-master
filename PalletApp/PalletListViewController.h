//
//  PalletListViewController.h
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Variables.h"
#import "StorePalletDetailViewController.h"
#import "ShippingPalletDetailViewController.h"
#import "PalletDetailsCell.h"

@interface PalletListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property NSArray *palletTypeArray;
@property NSString *palletType;

@property UITableView *dropDownTableView;
@property (weak, nonatomic) IBOutlet UITableView *palletListTableView;

@property (weak, nonatomic) IBOutlet UITextField *palletTypeTF;

- (IBAction)dropDownAction:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

- (IBAction)segmentedAction:(id)sender;

@end
