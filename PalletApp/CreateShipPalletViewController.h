//
//  CreateShipPalletViewController.h
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Variables.h"
#import "CreatePalletViewController.h"
#import "WebServices.h"

@interface CreateShipPalletViewController : UIViewController<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property int orderIndex;
@property int buttonIndex;

@property (weak, nonatomic) IBOutlet UITextField *shipPalletIdTF;
@property (weak, nonatomic) IBOutlet UITextField *orderTF;
@property (weak, nonatomic) IBOutlet UITextField *locationTF;

@property WebServices *webServices;
@property UITableView *dropDownTableView;

- (IBAction)shipScanButtonAction:(UIButton *)sender;
- (IBAction)doneButtonAction:(UIButton *)sender;
- (IBAction)dropDownAction:(UIButton *)sender;
- (IBAction)palletDoneAction:(UIButton *)sender;

@end
