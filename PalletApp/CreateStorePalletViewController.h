//
//  CreateStorePalletViewController.h
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServices.h"
#import "Variables.h"
#import "CreatePalletViewController.h"

@interface CreateStorePalletViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *storePalletIdTF;
@property (weak, nonatomic) IBOutlet UITextField *noOfBoxesTF;
@property (weak, nonatomic) IBOutlet UITextField *locationTF;
@property (weak, nonatomic) IBOutlet UITextField *productsTF;

@property WebServices *webServices;
@property UITableView *dropDownTableView;

- (IBAction)dropDownAction:(UIButton *)sender;
- (IBAction)storeScanButtonAction:(UIButton *)sender;
- (IBAction)doneButtonAction:(UIButton *)sender;

@end
