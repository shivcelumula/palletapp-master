//
//  CreateStorePalletViewController.m
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "CreateStorePalletViewController.h"

@interface CreateStorePalletViewController ()

@end

@implementation CreateStorePalletViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _webServices = [[WebServices alloc] init];
    
    _dropDownTableView=[[UITableView alloc]init];
    _dropDownTableView.dataSource=self;
    _dropDownTableView.delegate=self;
    _dropDownTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _dropDownTableView.backgroundColor = [UIColor lightGrayColor];
    [_dropDownTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [_dropDownTableView reloadData];
    [self.view addSubview:_dropDownTableView];
    _dropDownTableView.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    _storePalletIdTF.text = palletID;
    _noOfBoxesTF.text = noOfBoxes;
    _locationTF.text = location;
    _productsTF.text = product;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    palletID = _storePalletIdTF.text;
    noOfBoxes = _noOfBoxesTF.text;
    location = _locationTF.text;
    product = _productsTF.text;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [locationArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier  forIndexPath:indexPath] ;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text=[locationArray objectAtIndex:indexPath.row];
    cell.textLabel.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_dropDownTableView setHidden:YES];
    _locationTF.text = [locationArray objectAtIndex:indexPath.row];
        
}

- (IBAction)dropDownAction:(UIButton *)sender {
    
    [_locationTF resignFirstResponder];
    
    _dropDownTableView.frame = CGRectMake(54, 304, 173, 140);
    [_dropDownTableView reloadData];
    _dropDownTableView.hidden = NO;
    
}

- (IBAction)storeScanButtonAction:(UIButton *)sender {
    
    int randomNumber = 100 + rand() % (1000-100);
    _storePalletIdTF.text = [NSString stringWithFormat:@"SP0%d",randomNumber];
}


- (IBAction)doneButtonAction:(UIButton *)sender {
    
    if (!([_storePalletIdTF.text isEqualToString:@""] || [_noOfBoxesTF.text isEqualToString:@""] || [_locationTF.text isEqualToString:@""] || [_productsTF.text isEqualToString:@""])) {
        
//        [storePalletArray arrayByAddingObjectsFromArray:@[_storePalletIdTF.text, _noOfBoxesTF.text, _locationTF.text, _productsTF.text]];
        
        
        storePalletArray = [NSMutableArray arrayWithArray:@[_storePalletIdTF.text, _noOfBoxesTF.text, _locationTF.text, _productsTF.text]];
        
        if ([_webServices postStorePalletData:storePalletArray]) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Store Pallet has been created" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
            
            CreatePalletViewController *createPVC = (CreatePalletViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"CreatePalletViewController"];
            
            createPVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:createPVC animated:YES completion:nil];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Internet / Server error. Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter all details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_storePalletIdTF resignFirstResponder];
    [_noOfBoxesTF resignFirstResponder];
    [_locationTF resignFirstResponder];
    
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _dropDownTableView.hidden = YES;
}

#pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
    NSLog(@"%@", [segue identifier]);
    if ([[segue identifier] isEqualToString:@"Done"])
    {
        
    }
        
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }

@end
