//
//  StoreProductsViewController.m
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "StoreProductsViewController.h"

@interface StoreProductsViewController ()

@end

@implementation StoreProductsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    productsArray = [NSMutableArray arrayWithArray:@[@"Chicken", @"Cream Mix", @"Sriracha Sauce", @"Cheese", @"Buttermilk", @"Meat"]];
//    
//    prodPackUnitArray = [NSMutableArray arrayWithArray:@[@"20/80 Oz", @"100/25 Oz", @"100/20 Oz", @"25/120 Oz", @"25/40 Oz", @"20/40 Oz"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productsArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    
    static NSString *CellIdentifier = @"StoreProductsCell";
    ProductsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    cell.storeProductLbl.text = [productsArray objectAtIndex:indexPath.row];
    cell.storeUnitWeightLbl.text = [prodPackUnitArray objectAtIndex:indexPath.row];
    
    cell.storeProductLbl.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.storeProductLbl.textAlignment = NSTextAlignmentCenter;

    cell.storeUnitWeightLbl.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.storeUnitWeightLbl.textAlignment = NSTextAlignmentCenter;

    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BlankCell-Details.png"]];


    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    product = [productsArray objectAtIndex:indexPath.row];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
