//
//  ShippingPalletDetailViewController.m
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "ShippingPalletDetailViewController.h"

@interface ShippingPalletDetailViewController ()

@end

@implementation ShippingPalletDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _dropDownTableView=[[UITableView alloc]init];
    _dropDownTableView.dataSource=self;
    _dropDownTableView.delegate=self;
    _dropDownTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _dropDownTableView.backgroundColor = [UIColor lightGrayColor];
    [_dropDownTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [_dropDownTableView reloadData];
    [self.view addSubview:_dropDownTableView];
    _dropDownTableView.hidden = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
//    _palletIdLbl.text = [[shipPalletArray objectAtIndex:palletIndex] objectAtIndex:0];
//    _orderIdLbl.text = [[shipPalletArray objectAtIndex:palletIndex] objectAtIndex:1];
//    _locationTF.text = [[shipPalletArray objectAtIndex:palletIndex] objectAtIndex:2];
//    
    _palletIdLbl.text = [shipPalletArray  objectAtIndex:0];
    _orderIdLbl.text = [shipPalletArray  objectAtIndex:1];
    _locationTF.text = [shipPalletArray  objectAtIndex:2];
    
    
    [_productsTableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _dropDownTableView) {
        return [locationArray count];
    }
    else
        return [[[shipPalletArray objectAtIndex:palletIndex] objectAtIndex:3] count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    if (tableView == _dropDownTableView) {
        cell.textLabel.text = [locationArray objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
    }
    else
        cell.textLabel.text = [[[shipPalletArray objectAtIndex:palletIndex] objectAtIndex:3] objectAtIndex:indexPath.row];
    
    cell.textLabel.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_dropDownTableView setHidden:YES];
    _locationTF.text = [locationArray objectAtIndex:indexPath.row];
}

- (IBAction)dropDownAction:(UIButton *)sender {
    
    _dropDownTableView.frame = CGRectMake(167, 184, 134, 100);
    [_dropDownTableView reloadData];
    _dropDownTableView.hidden = NO;
}

- (IBAction)doneAction:(UIButton *)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Shipping pallet location has been updated" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
