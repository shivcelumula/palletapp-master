//
//  PalletDetailsCell.m
//  PalletApp
//
//  Created by Shivzz on 9/26/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "PalletDetailsCell.h"

@implementation PalletDetailsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
