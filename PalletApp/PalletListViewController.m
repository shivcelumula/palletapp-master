//
//  PalletListViewController.m
//  PalletApp
//
//  Created by Shivzz on 7/27/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "PalletListViewController.h"

@interface PalletListViewController ()

@end

@implementation PalletListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _palletTypeArray = @[@"Store Pallet", @"Ship Pallet"];
    
    _dropDownTableView=[[UITableView alloc]init];
    _dropDownTableView.dataSource=self;
    _dropDownTableView.delegate=self;
    _dropDownTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _dropDownTableView.backgroundColor = [UIColor lightGrayColor];
    [_dropDownTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [_dropDownTableView reloadData];
    [self.view addSubview:_dropDownTableView];
    _dropDownTableView.hidden = YES;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    _palletTypeTF.text = @"Store Pallet";
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _dropDownTableView) {
        return [_palletTypeArray count];
    }
    else if([_palletType isEqualToString:@"Ship Pallet"])
//        return [shipPalletArray count];
        return 1;
    
    else
//        return [storePalletArray count];
        return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"PalletDetailsCell";
    PalletDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (tableView == _dropDownTableView) {
        cell.textLabel.text = [_palletTypeArray objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor clearColor];
    }
    else if([_palletType isEqualToString:@"Ship Pallet"]) {
        cell.palletIDLbl.text = [shipPalletArray  objectAtIndex:0];
        cell.locationLbl.text = [locationArray objectAtIndex:indexPath.row];
    }
    else {
        cell.palletIDLbl.text = [storePalletArray  objectAtIndex:0];
        cell.locationLbl.text = [locationArray objectAtIndex:indexPath.row];
    }
    
    
    cell.palletIDLbl.font  = [UIFont fontWithName:@"Gill Sans" size:18.0];
    cell.palletIDLbl.textAlignment = NSTextAlignmentCenter;
    
    cell.locationLbl.font  = [UIFont fontWithName:@"Gill Sans" size:18.0];
    cell.locationLbl.textAlignment = NSTextAlignmentCenter;
    
    cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BlankCell-Details.png"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _dropDownTableView) {
//        _palletType = [_palletTypeArray objectAtIndex:indexPath.row];
//        _palletTypeTF.text = [_palletTypeArray objectAtIndex:indexPath.row];
//        [_dropDownTableView setHidden:YES];
//        [_palletListTableView reloadData];
    }
    else
    {
        palletIndex = (int)indexPath.row;

        if ([_palletType isEqualToString:@"Ship Pallet"]) {
            
            ShippingPalletDetailViewController *shipPalletDVC = (ShippingPalletDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"ShippingPalletDetailViewController"];
            
            shipPalletDVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:shipPalletDVC animated:YES completion:nil];
        }
        else
        {
            StorePalletDetailViewController *storePalletDVC = (StorePalletDetailViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"StorePalletDetailViewController"];
            
            storePalletDVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:storePalletDVC animated:YES completion:nil];
        }
    }
    
}


- (IBAction)segmentedAction:(id)sender {
    
    
    NSLog(@"%d", [_segmentControl selectedSegmentIndex]);
    
    if ([_segmentControl selectedSegmentIndex] == 0) {
        _palletType = @"Store Pallet";
    }
    else
    {
        _palletType = @"Ship Pallet";
    }
    
    [_palletListTableView reloadData];
}

- (IBAction)dropDownAction:(UIButton *)sender {
    
    _dropDownTableView.frame = CGRectMake(75, 112, 168, 60);
    [_dropDownTableView reloadData];
    _dropDownTableView.hidden = NO;
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _dropDownTableView.hidden = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
