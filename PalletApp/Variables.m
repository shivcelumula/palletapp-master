//
//  Variables.m
//  PalletApp
//
//  Created by Shivzz on 7/28/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import "Variables.h"

NSString *palletID = @"";
NSString *noOfBoxes = @"";
NSString *location = @"";
int locationIndex = 0;
NSString *product = @"";
NSString *order = @"";

NSMutableArray *locationArray = nil;
NSMutableArray *productsArray = nil;

NSMutableArray *prodPackUnitArray = nil;
NSMutableDictionary *productDic = nil;

NSMutableArray *orderProductArray = nil;
NSMutableArray *orderProductsListArray =nil;
