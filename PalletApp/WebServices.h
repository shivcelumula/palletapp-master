//
//  WebServices.h
//  PalletApp
//
//  Created by Celumula,Shiva Teja on 7/3/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Variables.h"
#import "WebConstants.h"

@interface WebServices : NSObject

-(BOOL)validateLogin:(NSString*)empID password:(NSString*)password;
-(BOOL)getMetaData;

-(BOOL)postStorePalletData:(NSMutableArray*)dataArray;
-(BOOL)postShipPalletData:(NSMutableArray *)shipPalletArray;


@end
