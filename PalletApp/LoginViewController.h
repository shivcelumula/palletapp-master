//
//  LoginViewController.h
//  PalletApp
//
//  Created by Shivzz on 7/24/14.
//  Copyright (c) 2014 Shivzz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmpHomeViewController.h"
#import "Variables.h"
#import "WebServices.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property WebServices *webServices;
@property UIActivityIndicatorView *indicator;

@property (weak, nonatomic) IBOutlet UITextField *empIdTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;

- (IBAction)loginActionButton:(UIButton *)sender;

@end
